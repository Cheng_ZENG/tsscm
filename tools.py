#!/usr/bin/env python
"""
Useful tools for data processing
"""

from __future__ import print_function
import numpy as np
from tsscm import Tsscm
from utility import makefig

from ase.io import read
import os
import re


def make_band_plot(indices, ):
    """Make the band plot

    Parameters
    ------------
    indices: 2-length tuple, list or numpy array
        Indices of the two atoms, the bond length of which is fixed
        during relaxation.
    """
    tsscm = Tsscm(indices=indices)
    status = tsscm.check_running_status()
    if not status[0]:
        raise RuntimeError("First step relaxation unfinished!")

    dAB, forces, energies = [], [], []
    initial, final = read("initial.traj"), read("final.traj")
    force_start = np.product(get_force_sign(indices, initial))
    # force_start = np.product(get_force_sign(indices[::-1], initial))
    energies.append(initial.get_potential_energy())
    forces.append(force_start)
    dAB.append(initial.get_distance(indices[0], indices[1]))
    regex = re.compile('^[0-9]*')
    dirs = os.walk(".").next()[1]
    for dir in dirs:
        rxn_traj = os.path.join('.', dir, 'qn.traj')
        # out_energy = os.path.join(_,dir, 'out.energy')
        if regex.match(dir) and os.path.exists(rxn_traj):
            atoms = read(rxn_traj)
            dAB.append(atoms.get_distance(indices[0], indices[1]))
            force = np.product(get_force_sign(indices, atoms))
            forces.append(force)
            energies.append(atoms.get_potential_energy())
    energies.append(final.get_potential_energy())
    force_end = np.product(get_force_sign(indices, final))
    forces.append(force_end)
    dAB.append(final.get_distance(indices[0], indices[1]))

    F = np.array(forces)
    E = np.array(energies) - energies[0]

    dAB, E, F = zip(*sorted(zip(dAB, E, F)))
    # visualization
    chemical_symbol = final.get_chemical_symbols()[indices[0]]
    fig, _ax = makefig(figsize=(6.5, 8.0), nh=1, nv=2, lm=0.13, vg=0.01)
    _ax[0].plot(dAB, E, 'o-', label="Energy", color="navy")
    # _ax[0].set_xlabel('$d_{AB}$ [$\\AA$]')
    _ax[0].xaxis.set_ticklabels([])
    _ax[0].xaxis.set_visible([])
    _ax[0].set_ylabel('$\\Delta E$ [eV]')
    _ax[0].axvline(x=dAB[np.argmax(E)], linewidth=2,
                   linestyle="--", color='grey')
    _ax[0].text(0.8, 1.02,
                '$E_b=%.2f$ eV, $\\Delta E = %.2f$ eV' % (max(E), E[-1]-E[0]),
                verticalalignment='bottom', horizontalalignment='right',
                transform=_ax[0].transAxes,
                color='k', fontsize=12)
    _ax[1].plot(dAB, F, '>:', label="Force", color="brown")
    _ax[1].axvline(x=dAB[np.argmax(E)], linewidth=2,
                   linestyle="--", color='grey')
    _ax[1].set_xlabel('$d_{AB}$ [$\\AA$]')
    _ax[1].set_ylabel('Force on %s [$eV/\\AA$]' % chemical_symbol)
    fig.savefig("EnergyForce.png")
    return


def get_force_sign(indices, atoms):
    pair = indices
    i_A, i_B = pair
    forces = atoms.get_forces(apply_constraint=False)
    A_pos, B_pos = atoms.positions[i_A], atoms.positions[i_B]
    A_force = forces[i_A]

    vec_A_to_B = B_pos-A_pos
    # sign = 1 means repelling force, otherwise attractive force
    sign = 1 if np.dot(A_force, vec_A_to_B) < 0 else -1

    return (sign, np.round(np.linalg.norm(A_force), 3))
