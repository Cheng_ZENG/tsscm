"""
Search simple transition state in heterogenesou caltalysis
using constrained minimization method
For example, consider one collective variable as the bond length of
two atoms in the model system, which, while seemingly simple, can represent
a great number of surface chemical reaction mechanisms, such as N2
dissociation, CO oxidation and Methyl hydrogenation.
"""
from __future__ import print_function, division
from ase.io import read, write
from ase.neb import NEB
from ase.parallel import paropen
from ase.constraints import FixBondLength

import os
import re
import shutil
import subprocess
from glob import glob
from string import Template
import tarfile
import numpy as np
from numpy.linalg import norm

__author__ = "Cheng Zeng"
__version__ = "0.3"
__email__ = "cheng_zeng1@brown.edu"
__credits__ = "Jongyoon Bae"


calc_text = """from gpaw import GPAW, FermiDirac, Mixer, PW
calc = GPAW(txt='out.txt',
            mode=PW(ecut=450),
            h=0.18,
            xc='RPBE',
            kpts=(6, 6, 1),
            occupations=FermiDirac(0.1),
            spinpol=False,
            maxiter=333,
            poissonsolver={'dipolelayer': 'xy'},
            symmetry={'point_group': False},)
"""

script = """#!/usr/bin/env python
from ase.io import read
from ase.constraints import FixAtoms, FixBondLength
from ase.optimize import BFGS as qn
from ase.parallel import paropen
from ase.calculators.emt import EMT

atoms = read('image_$imageNo.traj')
atoms.set_constraint($constraint)

# User specified calculor
$calc_text

atoms.set_calculator(calc)

# You can change the filenames, but we should also change
# the filenames in the function `get_image_forces` accordingly
dyn = qn(atoms, logfile='qn.log', trajectory='qn.traj')
dyn.run(fmax=0.05)
potentialenergy=atoms.get_potential_energy()

f = paropen('out.energy', 'w')
f.write(str(potentialenergy))
f.close()
"""

script_nm = """#!/usr.bin.env python
from ase.vibrations import Vibrations
from ase.constraints import FixAtoms
from ase.io import read
from gpaw import GPAW, FermiDirac, Mixer, PW
atoms=read("$file")
$calc_text
atoms.calc = calc
vib = Vibrations(atoms, name="vib-ts", indices=$indices)
vib.run()
vib.summary(log="vib-ts.txt")
vib.write_mode()
"""


class Tsscm(object):
    """
    Parameters
    ------------
    initial: str
            The initial trajectory. If None, find the trajectory name starting
            with "initial" (and ends with .traj).

    final:  str
            The final trajectory. If None, auto search the target file.

    indices: list or tuple with length two
            The atomic indices of the two atoms of interest whose bond length
            will be fixed during relaxation.

    n_images: int
            Number of interior images for the first interpolation

    calc_text: str
            User specified calculator. For our work, we use GPAW,
            so it should looks like:
            >>> from gpaw import GPAW, FermiDirac, Mixer, PW
            >>> calc = GPAW(txt='out.txt',
            >>> mode=PW(ecut=450),
            >>> h=0.18,
            >>> xc='RPBE',
            >>> kpts=(6, 6, 1),
            >>> occupations=FermiDirac(0.1),
            >>> spinpol=False,
            >>> maxiter=333,
            >>> poissonsolver={{'dipolelayer': 'xy'}},
            >>> symmetry={{'point_group': False}},)

    """

    def __init__(self, indices, initial=None, final=None, n_images=5,
                 calc_text=calc_text,):
        assert len(indices) == 2, "merely consider two atoms"
        self.indices = indices
        if initial is None:
            self.initial = glob("./initial*.traj")[0]
        else:
            self.initial = initial
        if final is None:
            self.final = glob("./final*.traj")[0]
        else:
            self.final = final
        self.n_images = n_images
        self.ForceMinimized = False
        self.refine_region = False
        self.calc_text = calc_text
        self.cwd = os.getcwd()

    def initial_interpolate(self, idpp=True):
        if os.path.isdir('./1/'):
            return
        n_images = self.n_images
        initial = read(self.initial)
        final = read(self.final)
        images = [initial]
        images += [initial.copy() for _ in range(n_images)]
        images += [final]
        neb = NEB(images)
        neb.interpolate('idpp') if idpp else neb.interpolate()
        command = "test ! -d  %i && mkdir `seq 1 1 %i`" % (n_images, n_images)
        subprocess.call(command, shell=True)
        for i in range(1, len(images)-1):
            write("%i/image_%s.traj" % (i, str(i)), images[i])

    def calc_individual_image(self, sleep=0.1):
        """Use the same calculator as that for the initial/final state"""
        calc_text = self.calc_text
        old_constraint = read(self.initial).constraints
        self.old_constraint = old_constraint
        new_constraint = FixBondLength(self.indices[0], self.indices[1])
        constraints = [old_constraint[0], new_constraint]
        template = Template(script)
        cwd = os.getcwd()
        dirs = os.walk(self.cwd).next()[1]
        dirs = " ".join(dirs)
        r1 = re.findall(r"\b[0-9]{1,2}\b", dirs)
        for i in range(1, len(r1)+1):
            if os.path.exists('%s/qn.traj' % i):
                continue
            os.chdir("%s/%s/" % (cwd, str(i)))
            with paropen("calc.py", "w") as f:
                f.write(template.substitute(constraint=constraints,
                                            calc_text=calc_text,
                                            imageNo=i))
            command = 'qsub_new_system -n 2 -c 24 -t ' +\
                '50:00:00 -m 60g calc.py'
            os.system(command)
            os.chdir(cwd)

    def get_image_forces(self, fmax=0.1):
        """Get force direction and magnitudes
        on the pair for all the intermediate  images."""
        dirs = os.walk(self.cwd).next()[1]
        dirs = " ".join(dirs)
        r1 = re.findall(r"\b[0-9]{1,2}\b", dirs)
        n_images = len(r1)
        ds, signs, forces = [], [], []
        for i in range(1, n_images+1):
            file = os.path.join('.', "%s" % str(i), 'out.energy')
            if not os.path.exists(file):
                return
            atoms = read("%s/qn.traj" % i)
            d, sign, force = self._get_force_sign(atoms)
            if signs != [] and sign*signs[-1] < 0:
                self.refine_region = [int(i)-1, int(i)]
            ds.append(d)
            signs.append(sign)
            forces.append(force)
        if min(forces) < fmax:    # eV \AA^{-1}
            self.ForceMinimized = True
            self.ForceMinNo = np.argmin(forces)+1
            self.ForceMin = min(forces)
        return signs, forces

    def _get_force_sign(self, atoms):
        pair = self.indices
        i_A, i_B = pair
        forces = atoms.get_forces(apply_constraint=False)
        A_pos, B_pos = atoms.positions[i_A], atoms.positions[i_B]
        A_force = forces[i_A]
        vec_A_to_B = B_pos-A_pos
        # sign = 1 means repelling force, otherwise attractive force
        sign = 1 if np.dot(A_force, vec_A_to_B) < 0 else -1
        d = np.linalg.norm(vec_A_to_B)
        return (d, sign, np.round(norm(A_force), 3))

    def refine_TS_region(self, idpp=False, backup=True):
        """Further interpolation between images before and
        after the TS state based on the force signs and magtitudes."""
        if self.ForceMinimized:
            return
        if not self.refine_region:
            return
        pair = self.indices
        i_A, i_B = pair
        i_before, i_after = self.refine_region
        self.i_before, self.i_after = i_before, i_after
        before_TS = read("%i/qn.traj" % i_before)
        after_TS = read("%i/qn.traj" % i_after)
        signs, forces = self.get_image_forces()
        force_before = forces[i_before-1]*signs[i_before-1]
        dAB_before = before_TS.get_distance(i_A, i_B)
        force_after = forces[i_after-1]*signs[i_after-1]
        dAB_after = after_TS.get_distance(i_A, i_B)
        # assume that the TS region contracted by i_before and
        # and i_after is harmonic
        slope = (dAB_before-dAB_after)/(force_before-force_after)
        dAB_star = dAB_before - force_before*slope
        image1, image2 = before_TS.copy(), after_TS.copy()
        del image1.constraints
        del image2.constraints
        image1.set_distance(i_A, i_B, dAB_star)
        image2.set_distance(i_A, i_B, dAB_star)
        images = [image1, image1, image2]
        neb = NEB(images)
        neb.interpolate()
        image = images[1]
        image.set_distance(i_A, i_B, dAB_star)
        image.positions[i_A][2] += 0.5
        image.positions[i_B][2] += 0.5
        if backup:
            os.system('test ! -d backup/ && mkdir backup/')
            for j in range(1, len(forces)+1):
                os.system('cp -r %i/ backup/' % j)
            self.archive_directory('backup')
        cwd = os.getcwd()
        for i in range(len(forces), i_before, -1):
            command = "mv %i/ %i/" % (i, int(i+1))
            subprocess.call(command, shell=True)
            os.chdir("./%i" % int(i+1))
            command = "mv image_%i.traj image_%i.traj" % (i, i+1)
            subprocess.call(command, shell=True)
            os.chdir(cwd)
        os.system("mkdir %i" % i_after)
        write("%i/image_%s.traj" % (i_after, str(i_after)), image)

    def normal_mode_analysis(self, indices):
        """Normal mode analysis for the image with
        converged forces."""
        calc_text = self.calc_text
        command = "test ! -d normal_mode && mkdir normal_mode"
        subprocess.call(command, shell=True)
        os.chdir("./normal_mode")
        no = self.ForceMinNo
        if self.ForceMinimized:
            file = "../%i/qn.traj" % no
        subprocess.call("cp %s ." % file, shell=True)
        template_nm = Template(script_nm)
        f = paropen("normal_mode.py", "w")
        f.write(template_nm.substitute(file=file,
                                       calc_text=calc_text, indices=indices))
        f.close()
        command = 'qsub_new_system -n 2 -c 24 -t 50:00:00 ' + \
            ' -m 60g normal_mode.py'
        os.system(command)

    def check_running_status(self):
        """Check where you are while using this method.
        The following structure shows how we can check the workflow
        of this module,

        First-step relaxation (1) --> force converged (2)-->
        normal mode analysis (3)
        """
        dirs = os.walk(self.cwd).next()[1]
        dirs = " ".join(dirs)
        r1 = re.findall(r"\b[0-9]{1,2}\b", dirs)
        n_images = len(r1)
        file_list = ["%i/out.energy" % i for i in range(1, n_images+1)]
        condition1 = np.all([os.path.exists(file) for file in file_list])
        condition2 = self.ForceMinimized
        condition3 = os.path.exists("normal_mode/vib-ts.txt")

        if condition1:
            if not condition2:
                return [1, 0, 0]
            else:
                if condition3:
                    return [1, 1, 1, ]
                else:
                    return [1, 1, 0, ]
        else:
            return [0, 0, 0, ]

    def archive_directory(self, source_dir):
        """This function is from Andy's amp.stats.bootstrap module"""
        source_dir = "backup"
        outputname = source_dir + '.tar.gz'
        if os.path.exists(outputname):
            raise RuntimeError('%s exists.' % outputname)
        with tarfile.open(outputname, 'w:gz') as tar:
            tar.add(source_dir)
        shutil.rmtree(source_dir)
