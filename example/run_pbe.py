#!/usr/bin/env python
#SBATCH --account=ap31-condo 
#SBATCH --time=50:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --partition=batch
#SBATCH --mem=4g

#######################################
## Starting with two trajectories:
## initial.traj and final.traj 
######################################

from tsscm import Tsscm

calc_text="""from gpaw import GPAW, FermiDirac, Mixer, PW
calc = GPAW(txt=None,
            mode=PW(ecut=450),
            xc='PBE',
            kpts=(6, 6, 1),
            occupations=FermiDirac(0.1),
            spinpol=False,
            maxiter=333,
            poissonsolver={'dipolelayer': 'xy'},
            symmetry={'point_group': False} )
"""

indices =[36,37]
tsscm = Tsscm(indices=indices, calc_text=calc_text)

### Step 1: initial interpolation  
tsscm.initial_interpolate(idpp=True)
tsscm.calc_individual_image()

status = tsscm.check_running_status()
while not status[0]:
	status = tsscm.check_running_status()

if not status[1]:
	tsscm.get_image_forces()
	tsscm.refine_TS_region(backup=False)
	tsscm.calc_individual_image()

status = tsscm.check_running_status()
while not status[0]:
	status = tsscm.check_running_status()

if not status[1]:
	tsscm.get_image_forces()
	tsscm.refine_TS_region(backup=False)
	tsscm.calc_individual_image()

status = tsscm.check_running_status()
while not status[0]:
	status = tsscm.check_running_status()

if not status[1]:
	tsscm.get_image_forces()
	tsscm.refine_TS_region(backup=False)
	tsscm.calc_individual_image()
